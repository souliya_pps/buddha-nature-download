import React, { useEffect } from 'react';
import Header from './components/Header';
import MainContent from './components/MainContent';
import Footer from './components/Footer';
import styled from 'styled-components';
import natureImage from './assets/background_content.png'; // Replace with the correct path to your image
import natureImageMobile from './assets/background_mobile.jpg'; // Replace with the correct path to your image

const AppContainer = styled.div`
  font-family: 'Roboto, Noto Sans Lao, sans-serif';
  text-align: center;
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  background: url(${natureImage}) no-repeat center center;
  background-size: cover; /* Default to cover */

  @media (max-width: 768px) {
    background: url(${natureImageMobile}) no-repeat center center;
    background-size: cover; /* Elongate background image for mobile screens */
  }
`;

const ContentWrapper = styled.div`
  flex: 1;
`;

const App: React.FC = () => {
  useEffect(() => {
    // Redirect to the external URL
    window.location.href = 'https://buddha-nature.web.app/';
  }, []);

  return (
    <AppContainer>
      <ContentWrapper>
        <Header />
        <MainContent />
      </ContentWrapper>
      <Footer />
    </AppContainer>
  );
};

export default App;
