import React from 'react';
import styled from 'styled-components';

const FooterContainer = styled.footer`
  background: linear-gradient(
    45deg,
    #7a5448,
    #bc6c46
  ); /* Gradient background */
  padding: 20px;
  color: white;
  text-align: center; /* Center align the text */
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2); /* Add shadow for depth */
`;

const FooterText = styled.p`
  font-size: 16px; /* Adjust font size */
  line-height: 1.6; /* Increase line height for better readability */
  color: white;
  margin: 0;
  padding: 10px 0;
  font-family: 'Roboto, Noto Sans Lao, sans-serif';
`;
const Footer: React.FC = () => {
  return (
    <FooterContainer>
      <FooterText>
        ແອັບຄຳສອນພຣະພຸດທະເຈົ້າ, ສ້າງຂື້ນເພື່ອເຜີຍແຜ່ໃຫ້ພວກເຮົາທັງຫຼາຍໄດ້ສຶກສາ
        ແລະ ປະຕິບັດຕາມ,
        ດັ່ງທີ່ພຣະຕະຖາຄົດກ່າວວ່າ "ທຳມະຍິ່ງເປີດເຜີຍຍິ່ງຮຸ່ງເຮືອງ" ເມື່ອໄດ້ສຶກສາ
        ແລະ ປະຕິບັດຕາມ ຈົນເຫັນທຳມະຊາດຕາມຄວາມເປັນຈິງ
        ກໍຈະຫຼຸດພົ້ນຈາກຄວາມທຸກທັງປວງ.
        <br />
        "ທຳກໍດີ ວິໄນກໍດີ ທີ່ເຮົາສະແດງແລ້ວ ບັນຍັດໄວ້ດີແລ້ວ ທຳ ແລະ ວິໄນນັ້ນ
        ຈະເປັນສາດສະດາແທນຕໍ່ໄປ"
      </FooterText>
    </FooterContainer>
  );
};

export default Footer;
