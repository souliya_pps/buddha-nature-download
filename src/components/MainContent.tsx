import React from 'react';
import styled from 'styled-components';
import androidImage from '../assets/android.png'; // Placeholder image
import iosImage from '../assets/ios.png'; // Placeholder image
import iosAddToHomeScreen from '../assets/iosAddToHomeScreen.jpg'; // Placeholder image
import laptopAddToHomeScreen from '../assets/laptopAddToHomeScreen.png'; // Placeholder image
import laptop from '../assets/laptop.png'; // Placeholder image

const MainContentContainer = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 40px 20px;
  background-size: cover;
  height: 100%;
`;

const CardContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  gap: 20px;
  width: 100%;
  justify-items: center; /* Center the grid items horizontally */

  @media (min-width: 768px) {
    grid-template-columns: 1fr 1fr;
  }

  @media (min-width: 992px) {
    grid-template-columns: 1fr 1fr 1fr;
  }

  @media (min-width: 1200px) {
    grid-template-columns: 1fr 1fr 1fr 1fr;
    gap: 10px; /* Reduced gap for closer cards */
  }
`;

const Card = styled.div`
  background-color: rgba(255, 255, 255, 0.8); /* Semi-transparent white */
  backdrop-filter: blur(10px); /* Apply blur effect */
  border-radius: 10px;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
  padding: 20px;
  text-align: center;
  max-width: 300px;
  width: 100%;
  margin: 0 auto; /* Center align the cards */

  @media (min-width: 360px) and (max-width: 740px) {
    max-width: 85%; /* Adjust max-width for screens between 360px and 740px */
    // position center
    margin-left: auto;
    margin-right: auto;
  }
`;

const AppTitle = styled.h3`
  font-size: 1.5rem;
  margin-bottom: 0px;
  margin-top: 5px;
`;

const AppImageAndroid = styled.img`
  width: 210px;
  height: auto;
  margin-bottom: -45px;
  margin-top: -50px;
  position: sticky;
`;

const AppImageIOS = styled.img`
  width: 150px;
  height: auto;
  margin-bottom: 5px;
`;

const AppImageLaptop = styled.img`
  width: 70px;
  height: auto;
  margin-bottom: -5px;
`;

const AppImage = styled.img`
  width: 100%; /* Set the width to 100% to make the image fill its container */
  height: auto; /* Maintain the aspect ratio */
  margin-bottom: 20px; /* Add margin for better spacing */
  max-width: 400px; /* Limit the maximum width of the image */
  object-fit: contain; /* Scale the image to fit within its container while preserving its aspect ratio */
  margin-bottom: -30px;
  margin-top: -50px;
  position: sticky;
`;

const AppImageAddToHomeScreen = styled.img`
  width: 100%; /* Set the width to 100% to make the image fill its container */
  height: auto; /* Maintain the aspect ratio */
  margin-bottom: 20px; /* Add margin for better spacing */
  max-width: 400px; /* Limit the maximum width of the image */
  object-fit: contain; /* Scale the image to fit within its container while preserving its aspect ratio */
  margin-bottom: -30px;
  margin-top: -50px;
  position: sticky;

  @media (max-width: 768px) {
    max-width: 100%; /* Ensure the image does not exceed the container width on smaller screens */
    margin-bottom: -20px; /* Adjust margins for smaller screens */
    margin-top: -40px;
  }
`;

const DownloadButton = styled.a`
  display: inline-block;
  background: linear-gradient(
    45deg,
    #7a5448,
    #bc6c46
  ); /* Gradient background */
  color: white;
  padding: 10px 20px;
  border-radius: 5px;
  text-decoration: none;
  transition: background 0.3s;
  font-weight: bold;
  margin-top: 10px;
  cursor: pointer; /* Ensure the cursor changes to pointer */
  z-index: 1; /* Ensure the button is above other elements */
  position: relative; /* Ensure the button is positioned relative to its container */
  touch-action: manipulation;
  
  &:hover {
    background: #5a3a2f;
  }

  i {
    margin-right: 8px;
  }

  /* Media query for mobile devices */
  @media (max-width: 600px) {
    padding: 15px 25px; /* Increase padding for easier clicking on mobile */
    font-size: 18px; /* Increase font size for better readability on mobile */
  }
`;

const YouTubeEmbed = styled.iframe`
  width: 100%;
  height: 200px;
  margin: 20px 0;
  border: none;
`;

const ContactInfo = styled.div`
  margin: 20px 0;
  font-weight: bold;
`;

const PhoneNumber = styled.a`
  font-size: 1.2rem;
  color: #333;
  margin: 10px 0;
  display: block;
  text-decoration: none;
  &:hover {
    text-decoration: underline;
  }
`;

const Email = styled.a`
  font-size: 1.2rem;
  color: #7a5448;
  text-decoration: none;
  display: block;
  &:hover {
    text-decoration: underline;
  }
`;

const MainContent: React.FC = () => {
  return (
    <MainContentContainer>
      <CardContainer>
        <Card>
          <AppImageAndroid src={androidImage} alt='Android app' />
          <DownloadButton
            href='https://buddha-nature.web.app'
            target='_blank'
            rel='noopener noreferrer'
          >
            <i className='fab fa-android'></i> ດາວໂຫຼດ Android 1
          </DownloadButton>
          <DownloadButton
            href='https://play.google.com/store/apps/details?id=com.buddha.lao_tipitaka&pcampaignid=web_share'
            target='_blank'
            rel='noopener noreferrer'
          >
            <i className='fab fa-android'></i> ດາວໂຫຼດ Android 2
          </DownloadButton>
          <DownloadButton
            href='https://apkpure.com/lao-buddhaword-%E0%BA%9E%E0%BA%A3%E0%BA%B0%E0%BA%9E%E0%BA%B8%E0%BA%94%E0%BA%97%E0%BA%B0%E0%BB%80%E0%BA%88%E0%BA%BB%E0%BB%89%E0%BA%B2/com.buddha.lao_tipitaka/downloading/43.0.0'
            target='_blank'
            rel='noopener noreferrer'
          >
            <i className='fab fa-android'></i> ດາວໂຫຼດ Old Version
          </DownloadButton>
          <YouTubeEmbed
            src='https://www.youtube.com/embed/Fs8KCFsRRdY?si=oYLd5_lJ8YrgPrU0'
            allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
            allowFullScreen
            title='YouTube video player'
          />
        </Card>
        <Card>
          <AppImageIOS src={iosImage} alt='iOS app' />
          <DownloadButton
            href='https://buddha-nature.web.app'
            target='_blank'
            rel='noopener noreferrer'
          >
            <i className='fab fa-apple'></i> ສຳຫຼັບລະບົບ IOS (Iphone)
          </DownloadButton>
          <div
            style={{
              marginTop: '70px',
            }}
          />
          <AppImage src={iosAddToHomeScreen} alt='Additional view' />

          <div
            style={{
              marginTop: '20px',
            }}
          />
          <YouTubeEmbed
            src='https://www.youtube.com/embed/zselTVtT70o?si=YgkTtAO6auCOl2Mf'
            allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
            allowFullScreen
            title='YouTube video player'
          />
        </Card>

        <Card>
          <AppImageLaptop src={laptop} alt='laptop' />
          <DownloadButton
            href='https://buddha-nature.web.app'
            target='_blank'
            rel='noopener noreferrer'
          >
            <i className='fas fa-laptop'></i> ສຳຫຼັບ Laptop PC Notebook
          </DownloadButton>
          <div
            style={{
              marginTop: '70px',
            }}
          />
          <AppImageAddToHomeScreen
            src={laptopAddToHomeScreen}
            alt='Additional view'
          />
        </Card>

        <Card>
          <AppTitle>ສົນທະນາທັມ</AppTitle>
          <DownloadButton
            href='https://chat.whatsapp.com/CZ7j5fhSatK37v76zmmVCK'
            target='_blank'
            rel='noopener noreferrer'
          >
            <i className='fab fa-whatsapp'></i> WhatsApp
          </DownloadButton>
          <br />
          <DownloadButton
            href='https://www.facebook.com/profile.php?id=100077638042542'
            target='_blank'
            rel='noopener noreferrer'
          >
            <i className='fab fa-facebook'></i> Facebook
          </DownloadButton>
          <br />
          <DownloadButton
            href='https://www.youtube.com/channel/UC4MMOgY-nI6JyxBS9KGwikQ/featured'
            target='_blank'
            rel='noopener noreferrer'
          >
            <i className='fab fa-youtube'></i> Youtube
          </DownloadButton>

          <div
            style={{
              marginTop: '35px',
            }}
          />
          <ContactInfo>
            <PhoneNumber href='tel:+8562078287509'>+8562078287509</PhoneNumber>
            <div
              style={{
                marginTop: '35px',
              }}
            />
            <PhoneNumber href='tel:+8562077801610'>+8562077801610</PhoneNumber>
            <div
              style={{
                marginTop: '35px',
              }}
            />
            <Email href='mailto:souliyappsdev@gmail.com'>
              souliyappsdev@gmail.com
            </Email>
            <div
              style={{
                marginTop: '35px',
              }}
            />
            <Email href='mailto:Katiya921@gmail.com'>Katiya921@gmail.com</Email>
          </ContactInfo>
        </Card>
      </CardContainer>
    </MainContentContainer>
  );
};

export default MainContent;
