import React from 'react';
import styled from 'styled-components';
import logo from '../assets/logo.png'; // Replace with the correct path to your logo

const HeaderContainer = styled.header`
  display: flex;
  justify-content: center; /* Center the content horizontally */
  align-items: center;
  background: linear-gradient(
    45deg,
    #7a5448,
    #bc6c46
  ); /* Gradient background */
  padding: 20px;
  color: white;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2); /* Add shadow for depth */
`;

const Logo = styled.img`
  width: 60px;
  height: 60px;
  margin-right: 10px;
`;

const Title = styled.h1`
  font-size: 1.5em;
  margin: 0;
`;

const Header: React.FC = () => {
  return (
    <HeaderContainer>
      <Logo src={logo} alt='Logo' />
      <Title>ດາວໂຫຼດ APP ຄຳສອນພຣະພຸດທະເຈົ້າ</Title>
    </HeaderContainer>
  );
};

export default Header;
